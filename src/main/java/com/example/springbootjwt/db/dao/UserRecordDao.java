package com.example.springbootjwt.db.dao;

import com.example.springbootjwt.db.model.UserRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRecordDao extends JpaRepository<UserRecord, Long> {
    Optional<UserRecord> findUserRecordByUsername(String username);
}
