package com.example.springbootjwt.security.controller;

import com.example.springbootjwt.dto.UserRecordDto;
import com.example.springbootjwt.security.service.JwtUserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);

    private final JwtUserDetailsService jwtUserDetailsService;

    public RegistrationController(JwtUserDetailsService jwtUserDetailsService) {
        this.jwtUserDetailsService = jwtUserDetailsService;
    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody UserRecordDto userRecordDto) {
        return ResponseEntity.ok(jwtUserDetailsService.save(userRecordDto).getUsername());
    }
}
