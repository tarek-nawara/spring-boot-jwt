package com.example.springbootjwt.security.service;

import com.example.springbootjwt.db.dao.UserRecordDao;
import com.example.springbootjwt.db.model.UserRecord;
import com.example.springbootjwt.dto.UserRecordDto;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;

@Component
public class JwtUserDetailsService implements UserDetailsService {
    private final UserRecordDao userRecordDao;
    private final PasswordEncoder passwordEncoder;

    public JwtUserDetailsService(UserRecordDao userRecordDao, PasswordEncoder passwordEncoder) {
        this.userRecordDao = userRecordDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserRecord> userRecordByUsername = userRecordDao.findUserRecordByUsername(username);
        return userRecordByUsername
                .map(userRecord -> new User(username, userRecord.getPassword(), new ArrayList<>()))
                .orElseThrow(() -> new UsernameNotFoundException("User not found with the given username"));
    }

    public UserRecord save(UserRecordDto userRecordDto) {
        UserRecord userRecord = new UserRecord(userRecordDto.getUsername(),
                this.passwordEncoder.encode(userRecordDto.getPassword()));
        return this.userRecordDao.save(userRecord);
    }
}
